/* eslint-disable react-hooks/exhaustive-deps */

import React, { useEffect, useState } from 'react';

const GameInfo = props => {

  const [symbol, setSymbol] = useState('X');

  useEffect(() => {
    props.game.subscribeOnPlayerChanged(setSymbol);
    if (symbol != null) {
      setSymbol('X');
    }
  }, [props]);

  return (
    <p>
      Your symbol is {symbol}
    </p>
  );
}

export default GameInfo;