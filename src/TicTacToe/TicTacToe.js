import React, { useState } from 'react';
import Cell from './Cell/Cell'
import './TicTacToe.scss';
import TicTacToeGame from './tic-tac-toe-game';
import GameInfo from './GameInfo/GameInfo';

const DEFAULT_DIMENSION = 3;

const TicTacToe = () => {
  const onVictory = symbol => setTimeout(() => alert(`${symbol} wins!`), 100);
  const onDraw = () => setTimeout(() => alert('Draw...'), 100);

  const [game, setGame] = useState(
    new TicTacToeGame(DEFAULT_DIMENSION, onVictory, onDraw)
  );

  const onDimensionChanged = dimension => setGame(
    new TicTacToeGame(dimension, onVictory, onDraw)
  );

  const onRandomClicked = () => game.placeSymbolAtRandom();

  const gameField = game.field.map((row, rowIdx) =>
    <div key={rowIdx}>
      {row.map((_, columnIdx) =>
        <Cell
          key={columnIdx}
          rowIdx={rowIdx}
          columnIdx={columnIdx}
          game={game}
        />
      )}
    </div>
  );

  return (
    <div className="TicTacToe">
      <div className="dimension-buttons">
        <button onClick={() => onDimensionChanged(3)}>3x3</button>
        <button onClick={() => onDimensionChanged(4)}>4x4</button>
        <button onClick={() => onDimensionChanged(5)}>5x5</button>
      </div>

      {gameField}
      
      <div className="random-button">
        <button onClick={onRandomClicked}>random</button>
      </div>
      <div>
        <GameInfo game={game} />
      </div>
    </div>
  );
};

export default TicTacToe;