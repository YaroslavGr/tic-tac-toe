/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect } from 'react';
import tic from './tic.png';
import tac from './tac.png';
import './Cell.scss';

const Cell = props => {

  const [symbol, setSymbol] = useState(null);

  useEffect(() => {
    props.game.subscribeOnCellChanged(props.rowIdx, props.columnIdx, setSymbol);
    if (symbol != null) {
      setSymbol(null);
    }
  }, [props]);

  const onClick = () => props.game.placeSymbol(props.rowIdx, props.columnIdx);

  const ticOrTac = (
    <img
      alt={symbol || 'empty cell'}
      src={symbol === 'X' ? tic : tac }
    />
  );

  return (
    <div
      className={`Cell ${!!symbol ? 'disabled' : ''}`}
      onClick={onClick}>
      {symbol ? ticOrTac : ''}
    </div>
  );
};

export default Cell;