const randomInteger = max => Math.floor(Math.random() * Math.floor(max));

export default class TicTacToeGame {
  
  constructor(dimension, onVictory, onDraw) {
    this._onCellChanged = Array(dimension).fill().map(_ => Array(dimension).fill());
    this.field = Array(dimension).fill().map(_ => Array(dimension).fill());
    this._onVictory = onVictory;
    this._onDraw = onDraw;
    this._symbol = 'X';
  }

  subscribeOnPlayerChanged = onPlayerChanged => this._onPlayerChanged = onPlayerChanged;

  subscribeOnCellChanged = (rowIdx, columnIdx, onCellChanged) =>
    this._onCellChanged[rowIdx][columnIdx] = onCellChanged;

  placeSymbol = (rowIdx, columnIdx) => {
    this.field[rowIdx][columnIdx] = this._symbol;
    this._onCellChanged[rowIdx][columnIdx](this._symbol);
    if (this._isWinner(this._symbol)) {
      this._onVictory(this._symbol);
    } else if (this._hasNoSpace()) {
      this._onDraw();
    }
    this._symbol = this._nextSymbol();
    this._onPlayerChanged(this._symbol);
  }

  placeSymbolAtRandom = () => {
    if (this._hasNoSpace()) {
      console.warn('Cannot place symbol: there is no space left');
      return;
    }
    const dimension = this.field.length;
    const rowIdx = randomInteger(dimension);
    const columnIdx = randomInteger(dimension);
    if (this.field[rowIdx][columnIdx] == null) {
      this.placeSymbol(rowIdx, columnIdx);
    } else {
      this.placeSymbolAtRandom();
    }
  }

  _nextSymbol = () => this._symbol === 'X' ? 'O' : 'X';

  _hasNoSpace = () =>
    this.field.map(row => row.every(element => element != null)).every(isFilled => isFilled);

  _isWinner = symbol => {
    const isSymbol = cell => cell === symbol;
    for (let i = 0; i < this.field.length; ++i) {
      if (this._row(i).every(isSymbol) || this._column(i).every(isSymbol)) {
        return true;
      }
    }
    return this._mainAxis().every(isSymbol)
        || this._secondaryAxis().every(isSymbol);
  }

  _row = index => this.field[index];

  _column = index => this.field.map(row => row[index]);
  
  _mainAxis = () => {
    let axisCells = [];
    for (let i = 0; i < this.field.length; ++i) {
      axisCells[i] = this.field[i][i];
    }
    return axisCells;
  }

  _secondaryAxis = () => {
    let axisCells = [];
    for (let i = 0; i < this.field.length; ++i) {
      axisCells[i] = this.field[i][this.field.length - i - 1];
    }
    return axisCells;
  }

}